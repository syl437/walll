import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {AppSettings} from '../../services/app-settings';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth-service";
import {ToastService} from "../../services/toast-service";

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})

export class LoginPage implements OnInit{

    registerForm: FormGroup;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public auth:AuthService,
                public toast:ToastService) {};

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }


    ngOnInit() {
        this.registerForm = new FormGroup({
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null),
        })
    }

    async onSubmit() {
        let id = await this.auth.loginUser('UserLogin' , this.registerForm.value);

        if(id == 0)
            this.toast.presentToast("נתונים לא נכונים אנא נסה שנית");
        else {
            this.auth.saveDetailToLocalstorage(this.registerForm.value.email, id);
            this.navCtrl.setRoot('HomePage');
        }
    }
}
