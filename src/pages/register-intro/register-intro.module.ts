import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterIntroPage } from './register-intro';

@NgModule({
  declarations: [
    RegisterIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterIntroPage),
  ],
})
export class RegisterIntroPageModule {}
