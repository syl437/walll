import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth-service";
import {ToastService} from "../../services/toast-service";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage implements OnInit {
    
    registerForm: FormGroup;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public auth:AuthService,
                public toast:ToastService) {
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }
    
    async ngOnInit() {
        this.registerForm = new FormGroup({
            'name':new FormControl(null,Validators.required),
            'phone':new FormControl(null,[Validators.required, Validators.minLength(9)]),
            'address':new FormControl(null),
            'email':new FormControl(null,[Validators.required, Validators.email]),
            'password':new FormControl(null),
            'info':new FormControl(null)
        })
    }
    
    
    async onSubmit()
    {
        console.log(this.registerForm.value);
        let id = await this.auth.registerUser('RegisterUser' , this.registerForm.value);

        if(id == 0)
            this.toast.presentToast("נתונים לא נכונים אנא נסה שנית");
        else {
            this.auth.saveDetailToLocalstorage(this.registerForm.value.email, id);
            this.navCtrl.setRoot('HomePage');
        }
    }
}
