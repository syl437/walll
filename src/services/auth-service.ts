import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {LoadingController} from "ionic-angular";
import { AppSettings } from './app-settings'
import {Storage} from '@ionic/storage';
import {Http} from "@angular/http";

@Injectable()
export class AuthService {
    
    constructor(public loadingCtrl: LoadingController,
                public storage:Storage,
                private http:Http) {}
    
    async registerUser (url:string, data:object) {
        return new Promise<any>(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            console.log("01" , AppSettings.SERVER_URL + '' + url);
            try {
                let body = new FormData();
                body.append('data', JSON.stringify(data));
                let userDetails = await this.http.post(AppSettings.SERVER_URL + '' + url, body).toPromise().then(response => response.json());
                resolve(userDetails);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                loading.dismiss();
            }
        });
    }

    async loginUser(url:string, data:object)
    {
        return new Promise<any>(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            console.log("01" , AppSettings.SERVER_URL + '' + url);
            try {
                let body = new FormData();
                body.append('data', JSON.stringify(data));
                let userDetails = await this.http.post(AppSettings.SERVER_URL + '' + url, body).toPromise().then(response => response.json());
                resolve(userDetails);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                loading.dismiss();
            }
        });
    }

    saveDetailToLocalstorage(mail:string , id:number)
    {
        localStorage.setItem("id",id.toString());
        localStorage.setItem("mail",mail);
    }
}


//this._categories.next(categories);
//resolve(this.categories);