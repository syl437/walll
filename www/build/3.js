webpackJsonp([3],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(280);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Documents\github\walll\src\pages\home\home.html"*/'<!--&lt;!&ndash;-->\n\n  <!--Generated template for the HomePage page.-->\n\n\n\n  <!--See http://ionicframework.com/docs/components/#navigation for more info on-->\n\n  <!--Ionic pages and navigation.-->\n\n<!--&ndash;&gt;-->\n\n<!--<ion-header>-->\n\n\n\n  <!--<ion-navbar>-->\n\n    <!--<ion-title>home</ion-title>-->\n\n  <!--</ion-navbar>-->\n\n\n\n<!--</ion-header>-->\n\n\n\n\n\n<!--<ion-content padding>-->\n\n\n\n<!--</ion-content>-->\n\n\n\n\n\n<!--&lt;!&ndash;Theme GMAPS + Location  Details &ndash;&gt;-->\n\n<!--&lt;!&ndash;Content &ndash;&gt;-->\n\n<!--<ion-content elastic-header has-header>-->\n\n  <!--<div id="elastic-header">-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n        <!--shay-->\n\n      <!--</ion-item>-->\n\n\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n  <!--</div>-->\n\n  <!--<ion-grid no-padding >-->\n\n    <!--<ion-row>-->\n\n      <!--<ion-col col-12 map-header>-->\n\n        <!--<ion-item no-lines transparent>-->\n\n          <!--test elastic header-->\n\n        <!--</ion-item>-->\n\n      <!--</ion-col>-->\n\n    <!--</ion-row>-->\n\n  <!--</ion-grid>-->\n\n<!--</ion-content>-->\n\n\n\n<!--<ion-content delegate-handle="example-scroller" elastic-header="example-elastic-header" overflow-scroll="false">-->\n\n  <!--<div id="example-elastic-header" class="background-image"></div>-->\n\n\n\n  <!--<div class="content">-->\n\n    <!--<div class="padding">-->\n\n      <!--<h1>Elastic header</h1>-->\n\n      <!--<p>An Ionic/Angular directive for elastic headers. This implementation causes no browser repaints with silky smooth 60fps as a result.</p>-->\n\n    <!--</div>-->\n\n    <!--<div class="list">-->\n\n      <!--<div class="item">1</div>-->\n\n      <!--<div class="item">2</div>-->\n\n      <!--<div class="item">3</div>-->\n\n      <!--<div class="item">4</div>-->\n\n      <!--<div class="item">5</div>-->\n\n      <!--<div class="item">6</div>-->\n\n      <!--<div class="item">7</div>-->\n\n      <!--<div class="item">8</div>-->\n\n      <!--<div class="item">9</div>-->\n\n      <!--<div class="item">10</div>-->\n\n      <!--<div class="item">12</div>-->\n\n      <!--<div class="item">13</div>-->\n\n      <!--<div class="item">14</div>-->\n\n      <!--<div class="item">15</div>-->\n\n      <!--<div class="item">1</div>-->\n\n      <!--<div class="item">2</div>-->\n\n      <!--<div class="item">3</div>-->\n\n      <!--<div class="item">4</div>-->\n\n      <!--<div class="item">5</div>-->\n\n      <!--<div class="item">6</div>-->\n\n      <!--<div class="item">7</div>-->\n\n      <!--<div class="item">8</div>-->\n\n      <!--<div class="item">9</div>-->\n\n      <!--<div class="item">10</div>-->\n\n      <!--<div class="item">12</div>-->\n\n      <!--<div class="item">13</div>-->\n\n      <!--<div class="item">14</div>-->\n\n      <!--<div class="item">15</div>-->\n\n      <!--<div class="item">1</div>-->\n\n      <!--<div class="item">2</div>-->\n\n      <!--<div class="item">3</div>-->\n\n      <!--<div class="item">4</div>-->\n\n      <!--<div class="item">5</div>-->\n\n      <!--<div class="item">6</div>-->\n\n      <!--<div class="item">7</div>-->\n\n      <!--<div class="item">8</div>-->\n\n      <!--<div class="item">9</div>-->\n\n      <!--<div class="item">10</div>-->\n\n      <!--<div class="item">12</div>-->\n\n      <!--<div class="item">13</div>-->\n\n      <!--<div class="item">14</div>-->\n\n      <!--<div class="item">15</div>-->\n\n      <!--<div class="item">1</div>-->\n\n      <!--<div class="item">2</div>-->\n\n      <!--<div class="item">3</div>-->\n\n      <!--<div class="item">4</div>-->\n\n      <!--<div class="item">5</div>-->\n\n      <!--<div class="item">6</div>-->\n\n      <!--<div class="item">7</div>-->\n\n      <!--<div class="item">8</div>-->\n\n      <!--<div class="item">9</div>-->\n\n      <!--<div class="item">10</div>-->\n\n      <!--<div class="item">12</div>-->\n\n      <!--<div class="item">13</div>-->\n\n      <!--<div class="item">14</div>-->\n\n      <!--<div class="item">15</div>-->\n\n      <!--<div class="item">1</div>-->\n\n      <!--<div class="item">2</div>-->\n\n      <!--<div class="item">3</div>-->\n\n      <!--<div class="item">4</div>-->\n\n      <!--<div class="item">5</div>-->\n\n      <!--<div class="item">6</div>-->\n\n      <!--<div class="item">7</div>-->\n\n      <!--<div class="item">8</div>-->\n\n      <!--<div class="item">9</div>-->\n\n      <!--<div class="item">10</div>-->\n\n      <!--<div class="item">12</div>-->\n\n      <!--<div class="item">13</div>-->\n\n      <!--<div class="item">14</div>-->\n\n      <!--<div class="item">15</div>-->\n\n      <!--<div class="item">1</div>-->\n\n      <!--<div class="item">2</div>-->\n\n      <!--<div class="item">3</div>-->\n\n      <!--<div class="item">4</div>-->\n\n      <!--<div class="item">5</div>-->\n\n      <!--<div class="item">6</div>-->\n\n      <!--<div class="item">7</div>-->\n\n      <!--<div class="item">8</div>-->\n\n      <!--<div class="item">9</div>-->\n\n      <!--<div class="item">10</div>-->\n\n      <!--<div class="item">12</div>-->\n\n      <!--<div class="item">13</div>-->\n\n      <!--<div class="item">14</div>-->\n\n      <!--<div class="item">15</div>-->\n\n    <!--</div>-->\n\n  <!--</div>-->\n\n<!--</ion-content>-->\n\n\n\n<!--<ion-header [elasticHeader]="myContent">-->\n\n  <!--<div id="example-elastic-header" class="background-image"></div>-->\n\n<!--</ion-header>-->\n\n<!--&lt;!&ndash;Add the fullscreen attribute and #<componentID> &ndash;&gt;-->\n\n<!--<ion-content class="home" fullscreen #myContent>-->\n\n  <!--<ion-list>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n     <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item>-->\n\n    <!--<ion-item >-->\n\n      <!--<ion-icon name="person" item-left></ion-icon>-->\n\n      <!--shay-->\n\n    <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item> <ion-item >-->\n\n    <!--<ion-icon name="person" item-left></ion-icon>-->\n\n    <!--shay-->\n\n  <!--</ion-item>-->\n\n\n\n\n\n\n\n\n\n\n\n<!--Add the fullscreen attribute and #<componentID> -->\n\n<ion-content >\n\n  <p appBasicHighlight>Style me with basic directive!</p>\n\n    <div class="header-image"></div>\n\n\n\n\n\n<div  class="main-content">\n\n  <div class="item">1</div>\n\n  <div class="item">2</div>\n\n  <div class="item">3</div>\n\n  <div class="item">4</div>\n\n  <div class="item">5</div>\n\n  <div class="item">6</div>\n\n  <div class="item">7</div>\n\n  <div class="item">8</div>\n\n  <div class="item">9</div>\n\n  <div class="item">10</div>\n\n  <div class="item">12</div>\n\n  <div class="item">13</div>\n\n  <div class="item">14</div>\n\n  <div class="item">15</div>\n\n  <div class="item">1</div>\n\n  <div class="item">2</div>\n\n  <div class="item">3</div>\n\n  <div class="item">4</div>\n\n  <div class="item">5</div>\n\n  <div class="item">6</div>\n\n  <div class="item">7</div>\n\n  <div class="item">8</div>\n\n  <div class="item">9</div>\n\n  <div class="item">10</div>\n\n  <div class="item">12</div>\n\n  <div class="item">13</div>\n\n  <div class="item">14</div>\n\n  <div class="item">15</div>\n\n  <div class="item">1</div>\n\n  <div class="item">2</div>\n\n  <div class="item">3</div>\n\n  <div class="item">4</div>\n\n  <div class="item">5</div>\n\n  <div class="item">6</div>\n\n  <div class="item">7</div>\n\n  <div class="item">8</div>\n\n  <div class="item">9</div>\n\n  <div class="item">10</div>\n\n  <div class="item">12</div>\n\n  <div class="item">13</div>\n\n  <div class="item">14</div>\n\n  <div class="item">15</div>\n\n  <div class="item">1</div>\n\n  <div class="item">2</div>\n\n  <div class="item">3</div>\n\n  <div class="item">4</div>\n\n  <div class="item">5</div>\n\n  <div class="item">6</div>\n\n  <div class="item">7</div>\n\n  <div class="item">8</div>\n\n  <div class="item">9</div>\n\n  <div class="item">10</div>\n\n  <div class="item">12</div>\n\n  <div class="item">13</div>\n\n  <div class="item">14</div>\n\n  <div class="item">15</div>\n\n  <div class="item">1</div>\n\n  <div class="item">2</div>\n\n  <div class="item">3</div>\n\n  <div class="item">4</div>\n\n  <div class="item">5</div>\n\n  <div class="item">6</div>\n\n  <div class="item">7</div>\n\n  <div class="item">8</div>\n\n  <div class="item">9</div>\n\n  <div class="item">10</div>\n\n  <div class="item">12</div>\n\n  <div class="item">13</div>\n\n  <div class="item">14</div>\n\n  <div class="item">15</div>\n\n  <div class="item">1</div>\n\n  <div class="item">2</div>\n\n  <div class="item">3</div>\n\n  <div class="item">4</div>\n\n  <div class="item">5</div>\n\n  <div class="item">6</div>\n\n  <div class="item">7</div>\n\n  <div class="item">8</div>\n\n  <div class="item">9</div>\n\n  <div class="item">10</div>\n\n  <div class="item">12</div>\n\n  <div class="item">13</div>\n\n  <div class="item">14</div>\n\n  <div class="item">15</div>\n\n</div>\n\n</ion-content>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n  <!--</ion-list>-->\n\n<!--</ion-content>-->'/*ion-inline-end:"C:\Users\USER\Documents\github\walll\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=3.js.map